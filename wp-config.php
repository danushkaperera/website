<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'manotiare' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(E|E4@M};<WJO#-h@18ZMMvRU&Uh>0j!@ua_cR<_ym?[Tn6,2~-:vYH<Cld:>&6L' );
define( 'SECURE_AUTH_KEY',  '8k/.2yL. }C[20a23u<t-6h{kv3/jF((o+$/my&sz-&pHpX0^S]P@`q0Rc.z|fc)' );
define( 'LOGGED_IN_KEY',    'egs|xGx54T0@#cGOFJ]()r?k`-KhzXQaxQ.AIy2Q]<;:j`mhu&vqao>?Pu1yw$d^' );
define( 'NONCE_KEY',        'jK;--PN4gyA@ym: yDt~5N:MsVid8T[cy}q/YQ^*2g]ne6:lO/})o]k@o)(z^]of' );
define( 'AUTH_SALT',        'RgI[D:J(2t#6WOsaOZtO o@Bz_Q}>?hZL&^~atQ7!m2o!^d&sk~7H2+4vnbrZdB-' );
define( 'SECURE_AUTH_SALT', 'rpdqP8_prEU(OSLLfcNNq}Zys2+{T@~.*jkYV5jk`TEhTt-hpc^x43Mnru2`ZQ.,' );
define( 'LOGGED_IN_SALT',   '.DN~$>2Nqx%1oq8aRTkS,t5N].YJ~;sXgxH)<X}IgEB`>H`}P=D!;f)q6H!X~`5L' );
define( 'NONCE_SALT',       'MMK {B.=+[MkODq2KKARW]/R{yG:+KbVJK[$3;0)g Ye{R/$4^}g.MXsNJD<[t;6' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

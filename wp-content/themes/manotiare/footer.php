<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.2
 */

?>


<div class="footer4-section bg-light section section-fluid section-padding">

    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="widget-contact">
                        <p class="email">info@manotiare.com.au</p>
                        <p class="phone">(+61) 422 169 443</p>
<!--                        <img class="learts-mt-10" src="--><?php //bloginfo('template_url'); ?><!--/assets/images/others/pay.png" alt="">-->
                        <p class="copyright learts-mt-40">&copy; 2020 Mano Tiare. All Rights Reserved</a></p>
                    </div>
                </div>




                <div class="col-sm-4">
                    <ul class="widget-list">
                        <li><a href="<?php echo site_url('/about-us'); ?>">About</a></li>
                        <li><a href="<?php echo site_url('/contact-us'); ?>">Store location</a></li>
                        <li><a href="<?php echo site_url('/contact-us'); ?>">Contact</a></li>

                    </ul>
                </div>



                <div class="col-sm-4">
                    <ul class="widget-list">
                        <li> <i class="fab fa-twitter"></i> <a href="https://www.twitter.com/">Twitter</a></li>
                        <li> <i class="fab fa-facebook-f"></i> <a href="https://www.facebook.com/">Facebook</a></li>
                        <li> <i class="fab fa-instagram"></i> <a href="https://www.instagram.com/">Instagram</a></li>
                        <!--  <li> <i class="fab fa-youtube"></i> <a href="https://www.youtube.com/">Youtube</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->


<!-- JS
============================================ -->
<?php wp_footer(); ?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->

<!-- Use the minified version files listed below for better performance and remove the files listed above -->
<script src="<?php bloginfo('template_url'); ?>/assets/js/vendor/vendor.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/plugins/plugins.min.js"></script>

<!-- Main Activation JS -->
<script src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>



</body>
</html>

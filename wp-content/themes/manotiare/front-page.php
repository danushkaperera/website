<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>



    <div class="offcanvas-overlay"></div>

    <!-- Slider main container Start -->
    <div class="home6-slider section">
        <div class="home6-slide-item" data-bg-image="<?php bloginfo('template_url'); ?>/assets/images/slider/home6/slide1-1.jpg">
            <div class="container">
                <div class="home6-slide1-content">
                    <h3 class="sub-title">Little Simple Things</h3>
                    <h2 class="title">Where Motivations Take Root</h2>
                    <div class="link"><a href="<?php echo site_url('/shop'); ?>" class="btn btn-light btn-hover-black">shop now</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider main container End -->





    <!-- Category Banner Section Start -->
    <div class="section section-fluid learts-pt-30">
        <div class="container">
            <div class="category-banner1-carousel">






                <div class="col">
                    <div class="category-banner1">
                        <div class="inner">
                            <a href="<?php echo site_url('/tivaivai'); ?>" class="image"><img src="<?php bloginfo('template_url'); ?>/assets/images/banner/category/banner-s1-1.jpg" alt=""></a>
                            <div class="content">
                                <h3 class="title">
                                    <a href="<?php echo site_url('/tivaivai'); ?>">tivaivai</a>
                                    <span class="number"></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="category-banner1">
                        <div class="inner">
                            <a href="<?php echo site_url('/sheets'); ?>" class="image"><img src="<?php bloginfo('template_url'); ?>/assets/images/banner/category/banner-s1-2.jpg" alt=""></a>
                            <div class="content">
                                <h3 class="title">
                                    <a href="<?php echo site_url('/sheets'); ?>">Bed Sheets</a>
                                    <span class="number"></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="category-banner1">
                        <div class="inner">
                            <a href="<?php echo site_url('/pareu'); ?>" class="image"><img src="<?php bloginfo('template_url'); ?>/assets/images/banner/category/banner-s1-3.jpg" alt=""></a>
                            <div class="content">
                                <h3 class="title">
                                    <a href="<?php echo site_url('/pareu'); ?>">Pareu</a>
                                    <span class="number"></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

         <!--        <div class="col">
                    <div class="category-banner1">
                        <div class="inner">
                            <a href="<?php echo site_url('/shop'); ?>" class="image"><img src="<?php bloginfo('template_url'); ?>/assets/images/banner/category/banner-s1-4.jpg" alt=""></a>
                            <div class="content">
                                <h3 class="title">
                                    <a href="<?php echo site_url('/shop'); ?>"></a>
                                    <span class="number"></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="category-banner1">
                        <div class="inner">
                            <a href="<?php echo site_url('/shop'); ?>" class="image"><img src="<?php bloginfo('template_url'); ?>/assets/images/banner/category/banner-s1-5.jpg" alt=""></a>
                            <div class="content">
                                <h3 class="title">
                                    <a href="<?php echo site_url('/shop'); ?>">Kniting & Sewing</a>
                                    <span class="number"></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
    <!-- Category Banner Section End -->

    <!-- Product Section Start -->
    <div class="section section-padding">
        <div class="container">

            <div class="row learts-mb-50">
                <div class="col">
                    <!-- Section Title Start -->
                    <div class="section-title text-center mb-0">
                        <h3 class="sub-title">Just for you</h3>
                        <h2 class="title title-icon-both">Select Your Choices</h2>
                    </div>
                    <!-- Section Title End -->
                </div>
            </div>

            <!-- Product Tab List Start -->
            <div class="row learts-mb-40">
                <div class="col">
                    <ul class="nav text-uppercase justify-content-center mx-n1 mb-n2">
                        <li class="nav-item mx-1 mb-2"><a href="#product-all" data-toggle="tab" class="btn btn-md btn-light btn-hover-primary active">Feature Products</a></li>
                        <!-- <li class="nav-item mx-1 mb-2"><a href="#product-gift-ideas" data-toggle="tab" class="btn btn-md btn-light btn-hover-primary">Tivaivai</a></li>
                        <li class="nav-item mx-1 mb-2"><a href="#product-home-decor" data-toggle="tab" class="btn btn-md btn-light btn-hover-primary">Sheets</a></li>
                        <li class="nav-item mx-1 mb-2"><a href="#product-kitchen" data-toggle="tab" class="btn btn-md btn-light btn-hover-primary">Pareu</a></li>
                        <li class="nav-item mx-1 mb-2"><a href="#product-toys" data-toggle="tab" class="btn btn-md btn-light btn-hover-primary">Kids</a></li> -->
                    </ul>
                </div>
            </div>
            <!-- Product Tab List End -->

            <div class="tab-content">
                <div class="tab-pane fade show active" id="product-all">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">


<?php
$args = array(
    'posts_per_page' => -1,
    'post_type'      => 'product',
    'post_status'    => 'publish',
    'tax_query'      => array(
        array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'featured',
            'operator' => 'IN',
            ),
        )  
);
$featured_product = new WP_Query( $args );

if ( $featured_product->have_posts() ) : 

while ( $featured_product->have_posts() ) : $featured_product->the_post();

         
     
$post_thumbnail_id     = get_post_thumbnail_id();
$product_thumbnail     = wp_get_attachment_image_src($post_thumbnail_id, $size = 'shop-feature');
$product_thumbnail_alt = get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true );
$product_price=get_post_meta($featured_product->id , 'sale_price',true);
$product_price=get_post_meta($featured_product->id , 'regular_price',true);

   $product = get_product( $featured_product->post->ID );
   $product_old_price = $product->regular_price;

   $price = $product->sale_price;

?>

    


                  



                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="<?php the_permalink(); ?>" class="image">
                                        <span class="product-badges">
                                            <span class="onsale">new</span>
                                        </span>
                                       <img src="<?php echo $product_thumbnail[0];?>" alt="<?php echo $product_thumbnail_alt;?>">
<!--                                  <img class="image-hover " src="" alt="Product Image">-->
                                    </a>
<!--                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>-->
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h6>
                                    <span class="price">
                                     <?php if($product_old_price){?>
                                        <span class="old"><?php echo $product_old_price; ?></span>
                                    <?php }?>
                                    <span class="new"><?php echo $price; ?></span>
                                    </span>
<!--                                    <div class="product-buttons">-->
<!--                                        <a href="#quickViewModal" data-toggle="modal" class="product-button hintT-top" data-hint="Quick View"><i class="fal fa-search"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Add to Cart"><i class="fal fa-shopping-cart"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Compare"><i class="fal fa-random"></i></a>-->
<!--                                    </div>-->
                                </div>
                            </div>
                        </div>



<?php 
endwhile; 
endif;         
wp_reset_query();
 ?>          


<?php

/**
 * Override theme default specification for product # per row
 */
function loop_columns() {
return 4; // 5 products per row
}
add_filter('loop_shop_columns', 'loop_columns', 8);

?>

                    </div>
                    <!-- Products End -->
                </div>


<!--      second section          -->

                <?php

                $arg_main = array('post_type' => 'post',
                    'tax_query' => array(
                        array(
                            'taxonomy' => '',
                            'field' => 'slug',
                            'terms' => array( 'featured' )
                        ),

                    )
                );
                // the query
                ?>
                <div class="tab-pane fade" id="product-gift-ideas">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">




                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="product-details.html" class="image">
                                        <span class="product-badges">
                                            <span class="onsale">-13%</span>
                                        </span>
                                        <img src="assets/images/product/s270/product-20.jpg" alt="Product Image">
                                    </a>
                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="product-details.html">Wooden Condiment Cups</a></h6>
                                    <span class="price">
                                        <span class="old">$45.00</span>
                                    <span class="new">$39.00</span>
                                    </span>
<!--                                    <div class="product-buttons">-->
<!--                                        <a href="#quickViewModal" data-toggle="modal" class="product-button hintT-top" data-hint="Quick View"><i class="fal fa-search"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Add to Cart"><i class="fal fa-shopping-cart"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Compare"><i class="fal fa-random"></i></a>-->
<!--                                    </div>-->
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Products End -->
                </div>



<!--end second section -->

                <div class="tab-pane fade" id="product-home-decor">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">



                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="product-details.html" class="image">
                                        <span class="product-badges">
                                            <span class="onsale">-13%</span>
                                        </span>
                                        <img src="assets/images/product/s270/product-1.jpg" alt="Product Image">
                                        <img class="image-hover " src="assets/images/product/s270/product-1-hover.jpg" alt="Product Image">
                                    </a>
                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="product-details.html">Boho Beard Mug</a></h6>
                                    <span class="price">
                                        <span class="old">$45.00</span>
                                    <span class="new">$39.00</span>
                                    </span>

                                </div>
                            </div>
                        </div>



                    </div>
                </div>


                <div class="tab-pane fade" id="product-kitchen">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">

                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="product-details.html" class="image">
                                        <span class="product-badges">
                                            <span class="onsale">-10%</span>
                                        </span>
                                        <img src="assets/images/product/s270/product-24.jpg" alt="Product Image">
                                        <img class="image-hover " src="assets/images/product/s270/product-24-hover.jpg" alt="Product Image">
                                    </a>
                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="product-details.html">Steel Watering Can</a></h6>
                                    <span class="price">
                                        <span class="old">$20.00</span>
                                    <span class="new">$18.00</span>
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>





                <div class="tab-pane fade" id="product-toys">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">

                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="product-details.html" class="image">
                                        <span class="product-badges">
                                            <span class="hot">hot</span>
                                        </span>
                                        <img src="assets/images/product/s270/product-23.jpg" alt="Product Image">
                                        <img class="image-hover " src="assets/images/product/s270/product-23-hover.jpg" alt="Product Image">
                                    </a>
                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="product-details.html">Round Tray Plate</a></h6>
                                    <span class="price">
                                        $100.00
                                    </span>
                                    <div class="product-buttons">
                                        <a href="#quickViewModal" data-toggle="modal" class="product-button hintT-top" data-hint="Quick View"><i class="fal fa-search"></i></a>
                                        <a href="#" class="product-button hintT-top" data-hint="Add to Cart"><i class="fal fa-shopping-cart"></i></a>
                                        <a href="#" class="product-button hintT-top" data-hint="Compare"><i class="fal fa-random"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div>
    </div>

    <!-- Deal of the Day Section Start -->
<!--     <div class="section section-fluid section-padding" data-bg-color="#f4f3ec">
        <div class="container">
            <div class="row align-items-center learts-mb-n30">

                <div class="col-lg-6 col-12 learts-mb-30">
                    <div class="product-deal-image text-center">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/product/deal-product-2.png" alt="">
                    </div>
                </div>

                <div class="col-lg-6 col-12 learts-mb-30">
                    <div class="product-deal-content">
                        <h2 class="title">Deal of the day</h2>
                        <div class="desc">
                            <p>Years of experience brought about by our skilled craftsmen could ensure that every piece produced is a work of art. Our focus is always the best quality possible.</p>
                        </div>
                        <div class="countdown1" data-countdown="2021/01/01"></div>
                        <a href="<?php echo site_url('/shop'); ?>" class="btn btn-dark btn-hover-primary">Shop Now</a>
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <!-- Deal of the Day Section End -->

    <!-- List Product Section Start -->
    <div class="section section-padding">
        <div class="container">
            <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 learts-mb-n30">



            </div>
        </div>
    </div>
    <!-- List Product Section End -->

    <!-- Instagram Section Start -->
    <div class="section section-fluid section-padding pt-0">
        <div class="container">

            <!-- Section Title Start -->
            <div class="section-title2 text-center">
                <h3 class="sub-title">Follow us on Instagram</h3>
                <h2 class="title">@Mano_Tiare</h2>
            </div>
            <!-- Section Title End -->

            <div id="instagram-feed221" class="instagram-carousel instagram-carousel1 instagram-feed">
            </div>

        </div>
    </div>
    <!-- Instagram Section End -->





<!---->
<!--<div id="primary" class="content-area">-->
<!--	<main id="main" class="site-main" role="main">-->

		<?php
		// Show the selected front page content.
//		if ( have_posts() ) :
//			while ( have_posts() ) :
//				the_post();
//				get_template_part( 'template-parts/page/content', 'front-page' );
//			endwhile;
//		else :
//			get_template_part( 'template-parts/post/content', 'none' );
//		endif;
		?>

		<?php
		// Get each of our panels and show the post data.
//		if ( 0 !== manotiare_panel_count() || is_customize_preview() ) : // If we have pages to show.
//
//
//			$num_sections = apply_filters( 'manotiare_front_page_sections', 4 );
//			global $manotiarecounter;
//
//			// Create a setting and control for each of the sections available in the theme.
//			for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
//				$manotiarecounter = $i;
//				manotiare_front_page_section( null, $i );
//			}

	//endif; // The if ( 0 !== manotiare_panel_count() ) ends here.
		?>

<!--	</main><!-- #main -->
<!--</div><!-- #primary -->

<?php
get_footer();

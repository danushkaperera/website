<?php
/**
Template Name: Pareu Page
 */

get_header();



?>


    <div class="offcanvas-overlay"></div>

    <!-- Page Title/Header Start -->
    <div class="page-title-section section" data-bg-image="<?php bloginfo('template_url'); ?>/assets/images/bg/page-title-1.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-title">
                        <h1 class="title">Pareu</h1>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Page Title/Header End -->

    <!-- Product Section Start -->
    <div class="section section-padding">
        <div class="container">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="product-all">
                    <!-- Products Start -->
                    <div class="products row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1">


<?php
$args = array(
    'posts_per_page' => -1,
    'post_type'      => 'product',
    'post_status'    => 'publish',
    'tax_query'      => array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'name',
            'terms'    => 'pareu',
            'operator' => 'IN',
            ),
        )  
);
$featured_product = new WP_Query( $args );

if ( $featured_product->have_posts() ) : 

while ( $featured_product->have_posts() ) : $featured_product->the_post();

         
     
$post_thumbnail_id     = get_post_thumbnail_id();
$product_thumbnail     = wp_get_attachment_image_src($post_thumbnail_id, $size = 'shop-feature');
$product_thumbnail_alt = get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true );
$product_price=get_post_meta($featured_product->id , 'sale_price',true);
$product_price=get_post_meta($featured_product->id , 'regular_price',true);

   $product = get_product( $featured_product->post->ID );
   $product_old_price = $product->regular_price;

   $price = $product->sale_price;

?>

    
           


                  



                        <div class="col">
                            <div class="product">
                                <div class="product-thumb">
                                    <a href="<?php the_permalink(); ?>" class="image">
                                        <span class="product-badges">
                                            <span class="onsale">sale</span>
                                        </span>
                                       <img src="<?php echo $product_thumbnail[0];?>" alt="<?php echo $product_thumbnail_alt;?>">
<!--                                  <img class="image-hover " src="" alt="Product Image">-->
                                    </a>
<!--                                    <a href="wishlist.html" class="add-to-wishlist hintT-left" data-hint="Add to wishlist"><i class="far fa-heart"></i></a>-->
                                </div>
                                <div class="product-info">
                                    <h6 class="title"><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h6>
                                    <span class="price">
                                     <?php if($product_old_price){?>
                                        <span class="old"><?php echo $product_old_price; ?></span>
                                    <?php }?>
                                    <span class="new"><?php echo $price; ?></span>
                                    </span>
<!--                                    <div class="product-buttons">-->
<!--                                        <a href="#quickViewModal" data-toggle="modal" class="product-button hintT-top" data-hint="Quick View"><i class="fal fa-search"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Add to Cart"><i class="fal fa-shopping-cart"></i></a>-->
<!--                                        <a href="#" class="product-button hintT-top" data-hint="Compare"><i class="fal fa-random"></i></a>-->
<!--                                    </div>-->
                                </div>
                            </div>
                        </div>



<?php endwhile; else:?>
	 	<div class="row learts-mb-50">
                <div class="col">
                    <!-- Section Title Start -->
                    <div class="section-title text-center mb-0">
                        <h3 class="sub-title">Sorry..!</h3>
                        <h2 class="title title-icon-both">New products comming soon...</h2>
                    </div>
                    <!-- Section Title End -->
                </div>
    	</div>
	<?php  
	endif;       
wp_reset_query();
 ?>          
				</div>
			</div>
		</div>

	</div>
</div>



<?php
get_footer();
?>
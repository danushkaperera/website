<?php
/*
    Template Name:shop

 */
get_header();
?>



    <div class="offcanvas-overlay"></div>

    <!-- Page Title/Header Start -->
    <div class="page-title-section section" data-bg-image="<?php bloginfo('template_url'); ?>/assets/images/bg/page-title-1.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-title">
                        <h1 class="title">shop</h1>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Page Title/Header End -->

    <!-- Contact Information & Map Section Start -->
    <div class="section section-padding">
        <div class="container">
            <?php the_content();	?>

        </div>
    </div>
    <!-- Contact Information & Map Section End -->


<?php
get_footer();
?>
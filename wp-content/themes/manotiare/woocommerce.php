<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 9/26/2020
 * Time: 9:40 PM
 */

 get_header(); ?>
    <div id="main" class="container-fluid">
        <div id="container" class="col-lg-12 col-sm-6 col-md-6 col-xs-12">
            <?php woocommerce_content(); ?>
        </div>
    </div>
<?php get_footer(); ?>